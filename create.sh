#!/bin/bash -xe
docker rm -f gen-cookbook || echo gen-cookbook does not exist, good
docker run -v $(pwd):/workspace --workdir /workspace --name gen-cookbook githor/latex:1.0 python bin/create.py 2>&1 | tee create_docker.log
rm -rf html
mkdir -p html
cp src/index* html
cp -r src/pictures html
rm src/*.html
if [ -e publish.sh ]; then
   bash -xe publish.sh
fi
