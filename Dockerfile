FROM centos:7.6.1810

RUN yum update -y \
	&& yum install -y \
		texlive \
		texlive-latex \
		texlive-bin-bin \
		texlive-latex-bin-bin \
		texlive-collection-xetex \
		texlive-multirow \
		texlive-pdftex-def \
		texlive-makeindex \
		texlive-mh \
		texlive-tex4ht-bin \
		graphviz \
		markdown \
		ImageMagick \
	&& adduser -u 1000 gendock

ADD Dockerfile /etc/Dockerfile

USER gendock
